<table border="0" cellpadding="0" cellspacing="0" height="auto" max-width="600px" width="100%">

  <tr>
    <td align="center">
	
      <!--[if gte mso 9]>
		<img src="[set img url]" width="600" height="339" border="0" style="display:block;padding: 0px;" />
			/* This is the image displayed when email client is MS Outlook; note that values of width and height attributes must be without units */
			
		<div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;">
	<![endif]-->
      
	  <img height="auto" max-width="600px" src="[set img url]" style="height: auto; width: 100%; display: block; padding: 0px; max-width: 600px;" width="100%"> 
	  /* this is the image displayed when the email client is NOT Outlook; note dimensions have units. */
	  
    <!--[if gte mso 9]> 
		</div> 
	<![endif]-->
	
    </td>
  </tr>
</table>